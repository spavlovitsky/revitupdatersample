﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Plumbing;
using Autodesk.Revit.UI;

namespace RevitUpdaterSample
{
    public class Main : IExternalApplication
    {
        public Result OnStartup(UIControlledApplication application)
        {
            // Register pipe updater
            PipeUpdater oPipeUpdater = new PipeUpdater(application.ActiveAddInId);
            UpdaterRegistry.RegisterUpdater(oPipeUpdater);

            //Get any pipe element
            var pipeFilter = new ElementClassFilter(typeof(Pipe));

            // Add update trigger
            UpdaterRegistry.AddTrigger(oPipeUpdater.GetUpdaterId(), pipeFilter,
                                        Element.GetChangeTypeElementAddition());
            return Result.Succeeded;
        }
        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }

        public class PipeUpdater : IUpdater
        {
            static AddInId m_appId;
            static UpdaterId m_updaterId;

            // constructor takes the AddInId for the add-in associated with this updater
            public PipeUpdater(AddInId id)
            {
                m_appId = id;
                m_updaterId = new UpdaterId(m_appId, new Guid("1D7EE715-22A3-43F6-8A3E-9F8E28EF7A76"));
            }

            public void Execute(UpdaterData updaterData)
            {
                Document doc = updaterData.GetDocument();

                //Check pipes for zero slope
                foreach (ElementId addedElemId in updaterData.GetAddedElementIds())
                {
                    var oPipe = doc.GetElement(addedElemId) as Pipe;

                    var pipeSlope = oPipe.get_Parameter(BuiltInParameter.RBS_PIPE_SLOPE).AsDouble();
                    if(pipeSlope == 0)
                    {
                        TaskDialog.Show("Ошибка", "Труба не может быть с нулевым уклоном");
                    }
                }
            }

            public string GetAdditionalInformation()
            {
                return "Pipe updater sample";
            }

            public ChangePriority GetChangePriority()
            {
                return ChangePriority.MEPAccessoriesFittingsSegmentsWires;
            }

            public UpdaterId GetUpdaterId()
            {
                return m_updaterId;
            }

            public string GetUpdaterName()
            {
                return "Pipe updater";
            }
        }
    }
}
